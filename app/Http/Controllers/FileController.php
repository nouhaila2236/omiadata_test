<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    public function index(Request $request)
    {
        $path = $request->get('path');

        // Check if the path exists
        if (File::exists($path)) {
            // Get all files and directories within the path
            $items = glob($path . '/{*,**/*}', GLOB_BRACE);

            $files = [];
            $directories = [];

            // Iterate over each item and categorize them as file or directory
            foreach ($items as $item) {
                if (is_file($item)) {
                    $files[] = $item;
                } elseif (is_dir($item)) {
                    $directories[] = $item;
                }
            }

            return response()->json([
                'files' => $files,
                'directories' => $directories,
            ]);
        }

        return response()->json(['message' => 'Path not found'], 404);
    }
}


