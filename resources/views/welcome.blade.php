<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel Vue File List</title>

    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <file-list></file-list>
    </div>

    <script src="/js/app.js"></script>
</body>
</html>
