import { createApp } from 'vue';
import FileList from './components/FileList.vue';
import Toast from 'vue-toastification';
import 'vue-toastification/dist/index.css';


const app = createApp({});
app.component('file-list', FileList);
app.use(Toast);
app.mount('#app'); // Replace #app with the ID of your root element
