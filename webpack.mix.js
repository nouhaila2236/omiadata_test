const mix = require('laravel-mix');
const path = require("path");

mix
    .webpackConfig({
        resolve: {
        alias: {
            "@/js": path.resolve(__dirname, "resources/js"),
            "@/components": path.resolve(__dirname, "resources/js/components"),
        },
        },
        module: {
        rules: [
            {
            test: /\.ts$/,
            loader: "ts-loader",
            exclude: /node_module/,
            options: {
                appendTsSuffixTo: [/\.vue$/],
                transpileOnly: true,
            },
            },
        ],
        },
    })
    .js('resources/js/app.js', 'public/js')
    .vue({ version: 3 })
    ;
